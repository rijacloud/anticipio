<?php 

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\BackpackUser as User;
use Illuminate\Support\Facades\Hash;
use Auth;

class AuthController extends Controller {

    public function login(LoginRequest $request) {

        
        /**
         * - formulaire de connexion <
        * - récupérer l'email et le mot de passe  < 
        * - vérifier si l'email existe dans la bdd <
        * - si l'émail existe, on vérifie si l'email correspond au mot de passe ( manao comparaison ) < 
        * - si l'email n'existe pas,  on reviens vers la page de connexion avec une erreur " l'email entrée n'existe pas " < 
        * - une fois l'email et le mot de passe vérifier on crée la session de l'utilisateur puis on le redirige 
            */

        $user = User::where('email', $request->get('email'))->first();

        $password_a_comparer = Hash::check($request->get('password'), $user->password); //true or false

        //test 
        //test $2y$10$qePlAj8KTBb7w6/c3.F/Ze5PTkAgeck/BPF9YjRb.H6rCpLkk2yM.

        if($password_a_comparer) { //true

            if (Auth::attempt( ['email' => $request->get('email'), 'password' => $request->get('password')] )) //Si tout va bien, on crée la session 
            {
                return redirect('/profil'); // et on redirige vers la page profile 
            }

        } else {
            return back()->withError('error','Le mot de passe de correspond pas');
        }

    }

}