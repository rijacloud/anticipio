<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\InstitutionsRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class InstitutionsCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class InstitutionsCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Institutions');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/institutions');
        $this->crud->setEntityNameStrings('institutions', 'institutions');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
//        $this->crud->setFromDb();
            $this->crud->addColumn([
                'name' => 'denomination',
                'type' => 'text',
                'label' => 'Nom'
            ]);

            $this->crud->addColumns([
                [
                    'name' => 'province',
                    'type' => 'text',
                    'label' => 'province'
                ]
            ]);


        }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(InstitutionsRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        ///$this->crud->setFromDb();

        $this->crud->addField([
            'name' => 'email',
            'label' => "Votre email", 
            'type' => 'email'
        ]);

        $this->crud->addField([
            // n-n relationship
            'label' => "Administrateur", // Table column heading
            'type' => "select2",
            'name' => 'admin_id', // the column that contains the ID of that connected entity
            'entity' => 'admin', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\Models\BackpackUser", // foreign key model
            'placeholder' => "Choississez un administrateur", // placeholder for the select
            'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
            // 'include_all_form_fields'  => false, // optional - only send the current field through AJAX (for a smaller payload if you're not using multiple chained select2s)
         ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

}
